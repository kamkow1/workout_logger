<?php

foreach(file("db_credentials.sh") as $line) {
    $pair = explode("=", $line, 2);
    $pair[1] = trim(str_replace("'", "", $pair[1]));
    define($pair[0], $pair[1]);
}

if (!isset($_POST["exercise"])) {
    exit("nie wybrano ćwiczenia");

}

if (!isset($_POST["reps"])) {
    exit("nie wybrano powtórzeń");

}

if (!isset($_POST["weight_kg"])) {
    exit("nie wybrano ciężaru");
}


$conn = new mysqli(DB_HOST_LOCAL, DB_USER, DB_PASSWORD, DB_NAME);

$exercise = $_POST["exercise"];
$reps = $_POST["reps"];
$weight_kg = $_POST["weight_kg"];

$conn->query("insert into Logs (log_date, exercise_id, weight_kg, reps) values (CURDATE(), $exercise, $weight_kg, $reps);");

header("Location: /kamil/workout_logger.php");

?>
