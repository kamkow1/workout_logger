
create table if not exists Exercises (
    id int not null auto_increment,
    name varchar(255),
    
    primary key(id)
);

create table if not exists Logs (
    id int not null auto_increment,
    reps int(2),
    log_date date,
    exercise_id int,
    weight_kg float,

    primary key(id),
    foreign key (exercise_id) references Exercises(id)
);

