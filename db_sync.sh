. ./db_credentials.sh

set -xe

mysql -u "$DB_USER" -h "$DB_HOST" -P "$DB_PORT" -D "$DB_NAME" -p"$DB_PASSWORD" < ./db.sql
