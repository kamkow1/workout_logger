<html lang="pl">
<head>
    <meta charset="UTF-8" />
    <link rel="stylesheet" href="style.css" />
    <title>Logger ćwiczeń Kamila Kowalczyk</title>
</head>
<body>

<div>
    <canvas id="exercise_chart" width="800" height="600"></canvas>
</div>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<?php

ini_set("error_reporting", E_ALL);
ini_set("display_errors", true);

foreach(file("db_credentials.sh") as $line) {
    $pair = explode("=", $line, 2);
    $pair[1] = trim(str_replace("'", "", $pair[1]));
    define($pair[0], $pair[1]);
}

$conn = new mysqli(DB_HOST_LOCAL, DB_USER, DB_PASSWORD, DB_NAME);

$sql = "select Logs.log_date, Exercises.name, Logs.reps, Logs.weight_kg from Logs join Exercises on Exercises.id = Logs.exercise_id";
if (isset($_GET["exercise"])) {
    $prefix = array_search("exercise", array_keys($_GET)) == 0 ? "where" : "and";
    $sql .= " $prefix Exercises.name = '$_GET[exercise]'";
}
if (isset($_GET["date"])) {
    $prefix = array_search("date", array_keys($_GET)) == 0 ? "where" : "and";
    $sql .= " $prefix Logs.log_date = '$_GET[date]'";
}
$sql .= " order by Logs.log_date desc, Exercises.name;";

$logs_result = $conn->query($sql);

$exercises_result = $conn->query("select Exercises.id, Exercises.name from Exercises;");

$table_headers = array("Date", "Exercise", "Repetitions", "Weight [kg]");

echo "<p>Sort by exercise:";
echo "<select id=\"exercise_filter\">";
echo "<option>All</option>";
while($row = $exercises_result->fetch_array()) {
    echo "<option>" . $row[1] . "</option>";
}
echo "</select>";
echo "</p>";

echo "<p>Sort by date:";
echo "<input type=\"date\" id=\"date\" />";
echo "</p>";

echo "<form action=\"add_data.php\" method=\"POST\">";
echo "<table>";
echo "<tr>";

foreach ($table_headers as $th) {
    echo "<th>" . $th . "</th>";
}

echo "</tr>";

if ($logs_result) {
    while($row = $logs_result->fetch_array()) {
        echo "<tr>";
        echo "<td class=\"date_column\">" . $row[0] . "</td>";
        echo "<td>" . $row[1] . "</td>";
        echo "<td class=\"rep_column\">" . $row[2] . "</td>";
        echo "<td class=\"weight_column\">" . ($row[3] == 0 ? "BW" : $row[3]) . "</td>";
        echo "</tr>";
    }
}

echo "<tr>";

echo "<td> <input type=\"submit\" value=\"Dodaj\" /> </td>";

echo "<td>";
echo "<select name=\"exercise\">";

if ($exercises_result) {
    $exercises_result->data_seek(0);
    while($row = $exercises_result->fetch_array()) {
        echo "<option value=\"" . $row[0] . "\">" . $row[1] . "</option>";
    }
}
echo "</select>";
echo "</td>";

echo "<td> <input type=\"number\" name=\"reps\" /> </td>";

echo "<td> <input type=\"number\" name=\"weight_kg\" step=0.01 /> </td>";

echo "</tr>";

echo "</table>";
echo "</form>";

?>

<script>

// filters

const exercise_filter = document.getElementById("exercise_filter");
exercise_filter.onchange = function(e) {
    const text = exercise_filter.options[exercise_filter.selectedIndex].text;
    let params = new URLSearchParams(window.location.search);
    params.set("exercise", text);
    window.location.search = params.toString();
}

const date = document.getElementById("date");
date.onchange = function(e) {
    let params = new URLSearchParams(window.location.search);
    params.set("date", date.value);
    window.location.search = params.toString();
}

// chart

const exercise_chart = document.getElementById("exercise_chart");
const ctx = exercise_chart.getContext("2d");


let params = new URLSearchParams(window.location.search);

if (params.get("exercise") != null) {
    let dates_column = document.getElementsByClassName("date_column");
    let dates = [];
    for (const date of dates_column) {
        dates.push(date.innerText);
    }
    dates.reverse();

    let weights_column = document.getElementsByClassName("weight_column");
    let weights = [];
    for (const weight of weights_column) {
        weights.push(weight.innerText == "BW" ? 0 : parseFloat(weight.innerText));
    }
    weights.reverse();
    
    let reps_column = document.getElementsByClassName("rep_column");
    let reps = [];
    for (const rep of reps_column) {
        reps.push(rep.innerText);
    }
    reps.reverse();

    let index = 0;

    new Chart(ctx, {
        type: 'line',
        data: {
            labels: dates,
            datasets: [{
                label: params.get("exercise") + " (Reps @ Weight [kg])",
                data: weights,
                borderWidth: 1
            }]
        },
        options: {
            plugins: {
                tooltip: {
                    mode: 'point',
                    callbacks: {
                    label: function(tooltip_item) {
                            return reps[tooltip_item.dataIndex] + " @ " + weights[tooltip_item.dataIndex];
                        }
                    }
                }
            },
            scales: {
                y: {
                    beginAtZero: true,
                    ticks: {
                        format: { maximumFractionDigits: 1, minimumFractionDigits: 1 }
                    }
                }
            }
        }
    });
} else {
    exercise_chart.style.display = "none";
}

</script>

</body>
</html>
